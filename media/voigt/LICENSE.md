*This LICENSE recursively applies to all media files in the same directory as the LICENSE file and its subdirectories.*

deep.TEACHING Media<br/>
by [Benjamin Voigt (deep.TEACHING - HTW Berlin)](https://www.htw-berlin.de/hochschule/personen/person/?eid=10919)<br/>
is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).<br/>
Based on a work at https://gitlab.com/deep.TEACHING.
