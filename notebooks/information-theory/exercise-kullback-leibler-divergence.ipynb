{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Information Theory - Kullback–Leibler Divergence"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Table of Contents\n",
    "* [Introduction](#Introduction)\n",
    "* [Requirements](#Requirements) \n",
    "  * [Knowledge](#Knowledge)\n",
    "  * [Modules](#Python-Modules)\n",
    "* [Exercise](#Exercise)\n",
    "* [Literature](#Literature)\n",
    "* [Licenses](#Licenses)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Introduction\n",
    "\n",
    "In this notebook you will use the KL Divergence to measure the quality of an approximation of a probability density function.\n",
    "\n",
    "In order to detect errors in your own code, execute the notebook cells containing `assert` or `assert_almost_equal`. These statements raise exceptions, as long as the calculated result is not yet correct."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Requirements\n",
    "\n",
    "### Knowledge\n",
    "\n",
    "To complete this exercise notebook you should possess knowledge about the following topics.\n",
    "* Proability mass function (pmf)\n",
    "* Proability density function (pdf)\n",
    "* Entropy\n",
    "* KL Divergence\n",
    "\n",
    "The following literature can help you to acquire this knowledge:\n",
    "* Read Chapter 3 \"Probability and Information Theory\" of the [Deep Learning Book](http://www.deeplearningbook.org/)\n",
    "* Lecture notes on Kullback Leibler Divergence by Jiawei Han [CS412 KL-divergence](http://hanj.cs.illinois.edu/cs412/bk3/KL-divergence.pdf)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Python Modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# External Modules\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import scipy.stats as ss\n",
    "from numpy.testing import assert_almost_equal\n",
    "\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Exercise"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Given are three probability density functions $p(x), q(x), pp(x)$. The values of x are in the interval $[0,1[$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "np.random.seed(42)\n",
    "\n",
    "a = np.random.rand(10)\n",
    "b = np.random.rand(15)\n",
    "ag = ss.gaussian_kde(a)\n",
    "bg = ss.gaussian_kde(b)\n",
    "k = np.linspace(0, 1, 10)\n",
    "p = ag(k) # pdf\n",
    "q = bg(k) # pdf"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "plt.plot(k, p)\n",
    "plt.title(\"Probability Density Function p\")\n",
    "plt.xlabel(\"x\")\n",
    "plt.ylabel(\"p(x)\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "plt.plot(k, q)\n",
    "plt.title(\"Probability Density Function q\")\n",
    "plt.xlabel(\"x\")\n",
    "plt.ylabel(\"q(x)\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "pp = np.ones_like(p) * 2.\n",
    "pp[k>0.5] = 0. # pdf"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "plt.plot(k, pp)\n",
    "plt.title(\"Probability Density Function pp\")\n",
    "plt.xlabel(\"x\")\n",
    "plt.ylabel(\"pp(x)\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "**Task:**\n",
    "\n",
    "Write a function for computing the KL Divergence for such a pdf (probability density function)\n",
    "in pure numpy. In order to successfully pass the tests, use the natural logarithm.\n",
    "\n",
    "**Reminder:**\n",
    "\n",
    "For probability mass functions / for discrete values:\n",
    "\n",
    "$$\n",
    "D_{KL}(Q \\mid \\mid P) = \\sum_{x \\in \\mathcal A x} Q(x) \\log \\frac{Q(x)}{P(x)}\n",
    "$$\n",
    "\n",
    "For probability density functions / for continous values:\n",
    "\n",
    "$$\n",
    "D_{KL}(Q \\mid \\mid P) = \\int_{-\\infty}^{+\\infty} q(x) \\log \\frac{q(x)}{p(x)} dx\n",
    "$$\n",
    "\n",
    "**Hint:**\n",
    "\n",
    "KL Divergence might not be defined for all elements in $p(x), q(x), pp(x)$. Find a workaround so your function still returns a useful value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# Implement this function\n",
    "\n",
    "def kl_div(q, p):\n",
    "    \"\"\"Calculates the KL Divergence D(q||p). \n",
    "    \n",
    "    :param q: values for the function q (true function)\n",
    "    :type q: ndarray containing n values of type float\n",
    "    :param p: values for the function p (approximation of q)\n",
    "    :type p: ndarray containing n values of type float\n",
    "\n",
    "    :returns: KL Divergence D(q||p) or np.infty if not possible\n",
    "    :rtype: float\n",
    "    \"\"\"\n",
    "    \n",
    "    raise NotImplementedError()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "kl_div(q, p)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# Executing this cell must not throw an AssertionError\n",
    "\n",
    "assert_almost_equal(kl_div(q, p), ss.entropy(q, p))\n",
    "assert_almost_equal(kl_div(pp, p), ss.entropy(pp, p))\n",
    "assert_almost_equal(kl_div(pp, q), ss.entropy(pp, q))\n",
    "assert_almost_equal(kl_div(p, pp), np.infty)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "**Task:**\n",
    "\n",
    "1. Explain why it's  possible to calculate `kl_div(pp, p)`,\n",
    " but not `kl_div(p, pp)`.\n",
    " \n",
    "2. Which value is larger `kl_div(pp, p)` or `kl_div(pp, q)`? What do you expect? Why?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Literature\n",
    "\n",
    "<table>\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"MAC03\"></a>[MAC03]\n",
    "        </td>\n",
    "        <td>\n",
    "            MacKay, David JC, and David JC Mac Kay. Information theory, inference and learning algorithms. Cambridge university press, 2003.\n",
    "        </td>\n",
    "    </tr>\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"GOO16\"></a>[GOO16]\n",
    "        </td>\n",
    "        <td>\n",
    "            Goodfellow, Ian, et al. Deep learning. Vol. 1. Cambridge: MIT press, 2016.\n",
    "        </td>\n",
    "    </tr>\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"HAN08\"></a>[HAN08]\n",
    "        </td>\n",
    "        <td>\n",
    "            J. Han. CS412 Fall 2008. \"Introduction to Data Warehousing and Data Mining\", 2.4.8 Kullback-Leibler Divergence, [Online] Available: http://hanj.cs.illinois.edu/cs412/bk3/KL-divergence.pdf [Accessed: 30-Jul-2019]\n",
    "        </td>\n",
    "    </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Licenses\n",
    "\n",
    "### Notebook License (CC-BY-SA 4.0)\n",
    "\n",
    "*The following license applies to the complete notebook, including code cells. It does however not apply to any referenced external media (e.g., images).*\n",
    "\n",
    "HTW Berlin - Angewandte Informatik - Advanced Topics - Exercise - Entropy <br/>\n",
    "by Christian Herta, Klaus Strohmenger<br/>\n",
    "is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).<br/>\n",
    "Based on a work at https://gitlab.com/deep.TEACHING.\n",
    "\n",
    "\n",
    "### Code License (MIT)\n",
    "\n",
    "*The following license only applies to code cells of the notebook.*\n",
    "\n",
    "Copyright 2018 Christian Herta, Klaus Strohmenger\n",
    "\n",
    "Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:\n",
    "\n",
    "The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\n",
    "\n",
    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "deep_teaching_kernel",
   "language": "python",
   "name": "deep_teaching_kernel"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
