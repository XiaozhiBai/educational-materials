{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "# Preprocessing the Dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Table of Contents\n",
    "* [Introduction](#Introduction)\n",
    "* [Requirements](#Requirements) \n",
    "  * [Modules](#Python-Modules)\n",
    "  * [Data](#Data)\n",
    "* [Dataset Generation](#Dataset-Generation)\n",
    "  * [Create Individual Files per WSI](#Create-Individual-Files-per-WSI)\n",
    "  * [Create Single File](#Create-Single-File)\n",
    "* [Summary and Outlook](#Summary-and-Outlook)\n",
    "* [Literature](#Literature)\n",
    "* [Licenses](#Licenses)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "In this notebook you will create your own dataset based on the CAMELYON16 data set solely (~700 giga bytes), as all positive WSIs come with xml files including coordinates of the tumorous regions. Subsequent notebooks will use this data set you will create now. \n",
    "\n",
    "Once you have finished this series of notebooks you can enhancing your implementation to also including the 50 positive WSIs of the CAMELYON17 training data set, which also come with xml files.\n",
    "\n",
    "The purpose of the preprocessing is the following:\n",
    "\n",
    "If we had enough RAM to store the whole data set, we would just load it once at the beginning of the training. But this is not the case. Reading the different WSI-files in their compressed tiff format every single time we train a new batch is very time consuming. So storing tiles with a fixed zoom level, fixed size, cropped and labeled in one single file, will save us a lot of time."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Requirements\n",
    "\n",
    "### Python-Modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Python Standard Library\n",
    "import random\n",
    "import sys\n",
    "import os\n",
    "\n",
    "# External Modules\n",
    "import numpy as np\n",
    "import h5py\n",
    "from datetime import datetime\n",
    "from skimage.filters import threshold_otsu\n",
    "from matplotlib import pyplot as plt\n",
    "\n",
    "\n",
    "from preprocessing.datamodel import SlideManager\n",
    "from preprocessing.processing import split_negative_slide, split_positive_slide, create_tumor_mask, rgb2gray\n",
    "from preprocessing.util import TileMap"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Data\n",
    "\n",
    "The data used in this notebook are from the CAMELYON data sets, which are freely available on the [CAMELYON data page](https://camelyon17.grand-challenge.org/Data/).\n",
    "\n",
    "The whole data sets have the following sizes:\n",
    "- CAMELYON16 *(~715 GiB)*\n",
    "- CAMELYON17 *(~2,8 TiB)*\n",
    "\n",
    "For this notebook to work the following file structure (for CAMELYON16) inside the data folder must be given:\n",
    "\n",
    "```\n",
    "data\n",
    "├── CAMELYON16\n",
    "│   ├── training\n",
    "│   │   ├── lesion_annotations\n",
    "│   │   │   └── tumor_001.xml - tumor_110.xml\n",
    "│   │   ├── normal\n",
    "│   │   │   └── normal_001.tif - normal_160.tif\n",
    "│   │   └── tumor\n",
    "│   │       └── tumor_001.tif - tumor_110.tif\n",
    "│   └── test\n",
    "│       ├── lesion_annotations\n",
    "│       │   └── test_001.xml - tumor_110.xml\n",
    "│       └── images\n",
    "│           └── test_001.tif - normal_160.tif\n",
    "│\n",
    "└── CAMELYON17\n",
    "    └── training\n",
    "        ├── center_0\n",
    "        │   └── patient_000_node_0.tif - patient_019_node_4.tif\n",
    "        ├── center_1\n",
    "        │   └── patient_020_node_0.tif - patient_039_node_4.tif\n",
    "        ├── center_2\n",
    "        │   └── patient_040_node_0.tif - patient_059_node_4.tif\n",
    "        ├── center_3\n",
    "        │   └── patient_060_node_0.tif - patient_079_node_4.tif\n",
    "        ├── center_4\n",
    "        │   └── patient_080_node_0.tif - patient_099_node_4.tif\n",
    "        ├── lesion_annotations\n",
    "        │   └── patient_004_node_4.xml - patient_099_node_4.xml\n",
    "        └── stage_labels.csv\n",
    "```\n",
    "\n",
    "**Note:** For the `SlideManager` class also uppercase and lowercase matters, especially to map annotations to tumor slides, so be consistant in file labeling. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Task:**\n",
    " \n",
    " If you have not done so far, download all remaining data of the CAMELYON16 data set and store it in a folder structure shown above."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dataset Generation\n",
    "\n",
    "In this notebook, we will use parts of the [data-handling-usage-guide.ipynb](./data-handling-usage-guide.ipynb) to create our own dataset. You have two options. We suggest to work through the whole application scenario with option (B) first.\n",
    "\n",
    "### Either Option\n",
    "\n",
    "- Process all files from the CAMELYON16 data set\n",
    "- No overlap for negative tiles\n",
    "- Minimum of 20% tissue in tiles for normal slides\n",
    "- Minimum of 60% tumorours tissue for positive slides\n",
    "\n",
    "### Option A\n",
    "\n",
    "- Slide zoom level 0 (0-9, 0 beeing the highest zoom)\n",
    "- Tile_size of 312x312\n",
    "- 156 pixel overlap for tumorous (positive tiles) since they are scarce\n",
    "- We  save up to 1000 tiles per slide\n",
    "- Processing in this notebook will take approximately ~60 hours [\\*]\n",
    "- Classifying the tiles of the WSIs of the test set will later take ~1 hour per WSI [\\*]\n",
    "\n",
    "### Option B\n",
    "\n",
    "- Slide zoom level 3 (0-9, 0 beeing the highest zoom)\n",
    "- Tile_size of 256x256\n",
    "- 128 pixel overlap for tumorous (positive tiles) since they are scarce\n",
    "- We only save up to 100 tiles per slide\n",
    "- Processing in this notebook will take approximately ~5 hours [\\*]\n",
    "- Training of CNN in the next Notebook will take ~10 hours [\\*]\n",
    "- Classifying the tiles of the WSIs of the test set will later take ~10 minutes per WSI [\\*]\n",
    "\n",
    "**Remark:**\n",
    "- [\\*] *[Tested on Xeon1231v3 @3.8Ghz, 16GB DDR3 @1666Hz, data set stored on magnetic harddrive]*\n",
    "- If you have the possibility to store the CAMELYON16 data set on SSD, do so.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "\n",
    "Most importantly, we will save all tiles from all WSIs into a single HDF5 file. This is crucial because when accessing the data later for training, most time is consumed when opening a file. Additionally, the training works better, when a single batch (e.g. 100 tiles), is as heterogenous as the original data. So when we want to read 100 random tiles, we ideally want to read 100 tiles from 100 different slides and we do not want to open 100 different files to do so.\n",
    "\n",
    "**Background Information:**\n",
    "\n",
    "Depending on the staining process and the slide scanner, the slides can differ quite a lot in color. Therefore a batch containing 100 tiles from one slide only will most likely prevent the CNN from generalizing well.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "### EDIT THIS CELL:\n",
    "### Assign the path to your CAMELYON16 data\n",
    "CAM_BASE_DIR = '/path-to-CAMELYON16-folder/'\n",
    "\n",
    "#example: absolute path for linux\n",
    "CAM_BASE_DIR = '/media/klaus/2612FE3171F55111/'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### Do not edit this cell\n",
    "CAM16_DIR = CAM_BASE_DIR + 'CAMELYON16/'\n",
    "GENERATED_DATA = CAM_BASE_DIR + 'output/'\n",
    "\n",
    "#example: output path may of course be different\n",
    "GENERATED_DATA = '/media/klaus/Toshiba/CAM16_output/'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "mgr = SlideManager(cam16_dir=CAM16_DIR)\n",
    "n_slides= len(mgr.slides)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "### Execute this cell for option A\n",
    "\n",
    "level = 0\n",
    "tile_size = 312"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "### Execute this cell for option B\n",
    "\n",
    "level = 3\n",
    "tile_size = 256"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### Execute this cell\n",
    "\n",
    "poi = 0.20 ### 20% of negative tiles must contain tissue (in contrast to slide backgroun)\n",
    "poi_tumor = 0.60 ### 60% of pos tiles must contain metastases\n",
    "### to not have too few positive tile, we use half overlapping tilesize\n",
    "overlap_tumor = tile_size // 2\n",
    "### we have enough normal tissue, so negative tiles will be less of a problem\n",
    "overlap = 0.0\n",
    "max_tiles_per_slide = 1000"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Hint:** \n",
    "\n",
    "* As mentioned above, the next two blocks will take alot of time, depending on the choosen option. Before starting to preprocess the full data set it might help to process just a few slides, e.g. two normal and two tumor, to test whether everything works as expected. \n",
    "* In some rare cases jupyter notebook can become unstable when running for hours. It might be a good idea to run the python program from shell instead. To do so export the notebook as python program. Go to `File --> Download as --> Python`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create Individual Files per WSI\n",
    "\n",
    "To make this process resumable if anything fails, we will first create one HDF5-File for each WSI. This way, if anything fails, like power failure, Python Kernel dying, you can just delete the very last file, which will most likely be corrupted, and resume the process by reexecuting the cells."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "tiles_pos = 0\n",
    "\n",
    "for i in range(len(mgr.annotated_slides)):\n",
    "    try: \n",
    "        \n",
    "        filename = '{}/{}_{}x{}_poi{}_poiTumor{}_level{}.hdf5'.format(GENERATED_DATA, mgr.annotated_slides[i].name, tile_size, tile_size, \n",
    "                                                       poi, poi_tumor, level)\n",
    "        # 'w-' creates file, fails if exists\n",
    "        h5 = h5py.File(filename, \"w-\", libver='latest')\n",
    "        \n",
    "        # create a new and unconsumed tile iterator\n",
    "        tile_iter = split_positive_slide(mgr.annotated_slides[i], level=level,\n",
    "                                         tile_size=tile_size, overlap=overlap_tumor,\n",
    "                                         poi_threshold=poi_tumor) \n",
    "\n",
    "        tiles_batch = []\n",
    "        for tile, bounds in tile_iter:\n",
    "            if len(tiles_batch) % 10 == 0: print('positive slide #:', i, 'tiles so far:', len(tiles_batch))\n",
    "            if len(tiles_batch) > max_tiles_per_slide: break\n",
    "            tiles_batch.append(tile)\n",
    "\n",
    "        # creating a date set in the file\n",
    "        dset = h5.create_dataset(mgr.annotated_slides[i].name, \n",
    "                                 (len(tiles_batch), tile_size, tile_size, 3), \n",
    "                                 dtype=np.uint8,\n",
    "                                 data=np.array(tiles_batch),\n",
    "                                 compression=0)   \n",
    "        h5.close()\n",
    "\n",
    "        tiles_pos += len(tiles_batch)\n",
    "        print(datetime.now(), i, '/', len(mgr.annotated_slides), '  tiles  ', len(tiles_batch))\n",
    "        print('pos tiles total: ', tiles_pos)\n",
    "\n",
    "    except:\n",
    "        print('slide nr {}/{} failed'.format(i, len(mgr.annotated_slides)))\n",
    "        print(sys.exc_info()[0])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tiles_neg = 0\n",
    "\n",
    "for i in range(len(mgr.negative_slides)): \n",
    "    try:\n",
    "        filename = '{}/{}_{}x{}_poi{}_poiTumor{}_level{}.hdf5'.format(GENERATED_DATA, mgr.negative_slides[i].name, tile_size, tile_size, \n",
    "                                                       poi, poi_tumor, level)\n",
    "        # 'w-' creates file, fails if exists\n",
    "        h5 = h5py.File(filename, \"w-\", libver='latest')\n",
    "        \n",
    "        # load the slide into numpy array\n",
    "        arr = np.asarray(mgr.negative_slides[i].get_full_slide(level=4))\n",
    "\n",
    "        # convert it to gray scale\n",
    "        arr_gray = rgb2gray(arr)\n",
    "\n",
    "        # calculate otsu threshold\n",
    "        threshold = threshold_otsu(arr_gray)\n",
    "\n",
    "        # create a new and unconsumed tile iterator\n",
    "        # because we have so many  negative slides we do not use overlap\n",
    "        tile_iter = split_negative_slide(mgr.negative_slides[i], level=level,\n",
    "                                         otsu_threshold=threshold,\n",
    "                                         tile_size=tile_size, overlap=overlap,\n",
    "                                         poi_threshold=poi)\n",
    "\n",
    "        tiles_batch = []\n",
    "        for tile, bounds in tile_iter:\n",
    "            if len(tiles_batch) % 10 == 0: print('neg slide:', i, 'tiles so far:', len(tiles_batch))\n",
    "            if len(tiles_batch) > max_tiles_per_slide: break\n",
    "            tiles_batch.append(tile)\n",
    "\n",
    "        # creating a date set in the file\n",
    "        dset = h5.create_dataset(mgr.negative_slides[i].name, \n",
    "                                 (len(tiles_batch), tile_size, tile_size, 3), \n",
    "                                 dtype=np.uint8,\n",
    "                                 data=np.array(tiles_batch),\n",
    "                                 compression=0)\n",
    "        h5.close()\n",
    "        \n",
    "        tiles_neg += len(tiles_batch)\n",
    "        print(datetime.now(), i, '/', len(mgr.negative_slides), '  tiles  ', len(tiles_batch))\n",
    "        print('neg tiles total: ', tiles_neg)\n",
    "        \n",
    "    except:\n",
    "        print('slide nr {}/{} failed'.format(i, len(mgr.negative_slides)))\n",
    "        print(sys.exc_info()[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create Single File\n",
    "\n",
    "\n",
    "Now we will create a new, and final HDF5 file to contain all tiles of all WSIs we just created. The benefit of this is to further reduce reading time, as opening a file needs some time and this way we just need to open one single file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "single_file = '{}/all_wsis_{}x{}_poi{}_poiTumor{}_level{}.hdf5'.format(GENERATED_DATA, tile_size, tile_size, \n",
    "                                                       poi, poi_tumor, level)\n",
    "h5_single = h5py.File(single_file, 'w')\n",
    "\n",
    "for f in os.listdir(GENERATED_DATA):\n",
    "    if f.startswith('normal_') or f.startswith('tumor_'):\n",
    "        filename = GENERATED_DATA + f\n",
    "        with h5py.File(filename, 'r') as h5:\n",
    "            for key in h5.keys():\n",
    "                print('processing: \"{}\", shape: {}'.format(key, h5[key].shape))\n",
    "                if h5[key].shape[0] > 0: ### dont create dsets for WSIs with 0 tiles\n",
    "                    dset = h5_single.create_dataset(key, \n",
    "                        h5[key].shape, \n",
    "                        dtype=np.uint8,\n",
    "                        data=h5[key][:],\n",
    "                        compression=0) \n",
    "            \n",
    "h5_single.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Summary and Outlook\n",
    "\n",
    "The next step is to train a neural network with the preprocessed data to be able to classify and predict unseen tiles.\n",
    "\n",
    "If you are curious how the `preprocessing` library you have used here works and how to use openslide, then take a look at the source code, it should not be too hard to understand the code. Note:\n",
    "* For negative slides: we use Otsu thresholding to distignuish between slide background and tissue\n",
    "* For positive slides: we just use the xml-files, which include polygons for metastatic regions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## Licenses\n",
    "\n",
    "### Notebook License (CC-BY-SA 4.0)\n",
    "\n",
    "*The following license applies to the complete notebook, including code cells. It does however not apply to any referenced external media (e.g., images).*\n",
    "\n",
    "create-custom-dataset<br/>\n",
    "by Klaus Strohmenger<br/>\n",
    "is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).<br/>\n",
    "Based on a work at https://gitlab.com/deep.TEACHING.\n",
    "\n",
    "\n",
    "### Code License (MIT)\n",
    "\n",
    "*The following license only applies to code cells of the notebook.*\n",
    "\n",
    "Copyright 2018 Klaus Strohmenger\n",
    "\n",
    "Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:\n",
    "\n",
    "The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\n",
    "\n",
    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "deep_teaching_kernel",
   "language": "python",
   "name": "deep_teaching_kernel"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
