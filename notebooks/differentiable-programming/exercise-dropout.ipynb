{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Dropout (Differentiable Programming)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Table of Contents\n",
    "\n",
    "* [Introduction](#Introduction)\n",
    "* [Requirements](#Requirements)\n",
    "  * [Knowledge](#Knowledge)\n",
    "  * [Python Modules](#Python-Modules)\n",
    "  * [Data](#Data)\n",
    "* [Dropout](#Dropout)\n",
    "* [Exercises](#Exercises)\n",
    "  * [Mask operator](#Mask-operator)\n",
    "  * [Forward pass](#Forward-pass)\n",
    "  * [Dropout layer](#Dropout-layer)\n",
    "  * [Expected value](#Expected-value)\n",
    "* [Literature](#Literature)\n",
    "* [Licenses](#Licenses)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "This notebook walks you through an implementation of a regularization technique called dropout. The idea is that in each forward pass during training, we randomly select units to 'drop out' from the network, i.e. remove them from the network. This forces the surviving units to learn without depending too heavily on the cooperation of other units and produce better results individually."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Requirements\n",
    "\n",
    "### Knowledge\n",
    "\n",
    "These are useful resources on the topic, though it's not required to read them entirely before tackling this notebook.\n",
    "\n",
    "* The original [Dropout paper](http://jmlr.org/papers/volume15/srivastava14a/srivastava14a.pdf) by Srivastava et al.<sup>[[SRI14]](#SRI14)</sup>\n",
    "* This [blog post](https://wiseodd.github.io/techblog/2016/06/25/dropout/) on dropout by Agustinus Kristiadi<sup>[[KRI16]](#KRI16)</sup>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Python Modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "from dp import NeuralNode,Node\n",
    "\n",
    "from sklearn import datasets,preprocessing\n",
    "from sklearn.model_selection import train_test_split\n",
    "\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Data\n",
    "\n",
    "This cell downloads the [breast cancer dataset](https://scikit-learn.org/stable/modules/generated/sklearn.datasets.load_breast_cancer.html#) provided by [sklearn](https://scikit-learn.org/stable/index.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x,y = datasets.load_breast_cancer(return_X_y=True)\n",
    "x = preprocessing.scale(x)\n",
    "x_train, x_test, y_train, y_test = train_test_split(x,y,test_size=0.2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dropout\n",
    "\n",
    "![Left: Units and connections in a neural net without dropout | Right: Units and connections of the net with dropout applied](https://i.ibb.co/0CvbBVW/Screenshot-2019-06-19-srivastava14a-pdf.png)\n",
    "> Figure 1 from 'Dropout:  A Simple Way to Prevent Neural Networks from Overfitting' [#SRI14](#SRI14)\n",
    "\n",
    "The authors of the [Dropout paper](#SRI4) propose that a good way to reduce overfitting is to average out the predictions of many separately trained networks - but this is too computationally expensive to do in practice.\n",
    "\n",
    "Introduce dropout: On the left, you see a network with all its units and their connections. On the right, the crossed out units have been dropped from the network along with all their connections. So it creates a new, 'thinned' version of the neural net.\n",
    "\n",
    "\n",
    "When we send train samples through the network in the forward pass, we randomly sample units to drop from the network. So for each sample we train a 'thinned' version of the net.\n",
    "This approximates training and averaging many different neural nets with shared parameters.\n",
    "\n",
    "The paper presents the following motivation ([#SRI14](#SRI14) p. 1932/p. 4 in the PDF)\n",
    "> \"Similarly, each hidden unit in a neural network trained with dropout must learn to work with a randomly chosen sample of other units.  This should make each hidden unit more robust and drive it towards creating useful features on its own without relying on other hidden units to correct its mistakes.  \"\n",
    "\n",
    "The following exercises walk you through an implementation of a dropout layer for a neural net."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercises\n",
    "### Mask operator\n",
    "\n",
    "**Task:**\n",
    "\n",
    "Implement a `mask` operator for the Node autodiff class.\n",
    "\n",
    "**Note:** Remember to implement the partial derivative of the mask operator since it's crucial for backprop. If a unit is killed through dropout, it doesn't contribute anything to the network. So the gradient that flows back into it should be 0."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def mask(self, mask : np.ndarray):\n",
    "    raise NotImplementedError()\n",
    "    return\n",
    "\n",
    "Node.mask = mask"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Verify your solution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# mask numbers 1..10 and square\n",
    "a = Node(np.arange(1,11)[None,:], 'A')\n",
    "mask = np.array([0, 1] * 5)\n",
    "b = a.mask(mask).square()\n",
    "\n",
    "\n",
    "# check gradients\n",
    "grads = b.grad(np.ones(b.shape))['A']\n",
    "assert grads[0,2] == 0\n",
    "assert grads[0,3] == 8\n",
    "assert grads[0,4] == 0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We'll again use the autodiff class to create a model for the breast cancer dataset. The method `linear_layer` adds a linear layer to the network, your task will be to implement a `dropout` layer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Model():\n",
    "    # define layers of the model\n",
    "    def __init__(self):\n",
    "        self.params = dict()\n",
    "        self.fc1 = self.linear(30,20,'fc1')\n",
    "        self.do1 = self.dropout(keep_prob=0.5)\n",
    "        self.fc2 = self.linear(20,10,'fc2')\n",
    "        self.do2 = self.dropout(keep_prob=0.5)\n",
    "        self.fc3 = self.linear(10,1,'fc3')\n",
    "    \n",
    "    # define forward pass\n",
    "    def forward(self,x,train=True):\n",
    "        if not type(x) == Node:\n",
    "            x = Node(x)\n",
    "        # TODO: implement forward pass\n",
    "        raise NotImplementedError()\n",
    "        return out\n",
    "        \n",
    "    # define loss function\n",
    "    def loss(self,x,y,train=True):\n",
    "        out = self.forward(x,train)\n",
    "        if not type(y) == Node:\n",
    "            y = Node(y)\n",
    "        loss = -1 * (y * out.log() + (1 - y) * (1 - out).log())\n",
    "        return loss.sum()\n",
    "    \n",
    "    # add a linear layer to the model\n",
    "    def linear(self, fan_in,fan_out,name):\n",
    "        W_name, W_value = f'weight_{name}', np.random.randn(fan_in,fan_out)\n",
    "        b_name, b_value = f'bias_{name}', np.random.randn(1,fan_out)\n",
    "        self.params[W_name] = W_value\n",
    "        self.params[b_name] = b_value\n",
    "        \n",
    "        def forward(x):\n",
    "            return x.dot(Node(self.params[W_name], W_name)) + Node(self.params[b_name], b_name)\n",
    "\n",
    "        return forward\n",
    "    \n",
    "    # TODO: add dropout method"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Forward pass\n",
    "\n",
    "**Task:**\n",
    "\n",
    "Implement the forward pass, e.g.\n",
    "```python\n",
    "x -> linear -> tanh -> dropout\n",
    "  -> linear -> tanh -> dropout\n",
    "  -> linear -> sigmoid\n",
    "```\n",
    "**Note:** Mind the `train` parameter which indicates whether we're forwarding train or test data. On test data, you do not apply dropout."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Dropout layer\n",
    "\n",
    "To apply dropout, we multiply the activations of a layer with a boolean/binary matrix of `0`s and `1`s (masking).\n",
    "\n",
    "The hyperparameter $p$ controls the percentage of units to keep. To make things more explicit, this parameter is also called `keep_prob`.\n",
    "\n",
    "Each dropout layer can have a different setting for the `keep_prob` parameter $\\in$ [0..1]\n",
    "\n",
    "**Task:**\n",
    "\n",
    "Implement the `dropout` layer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def dropout(self,keep_prob=0.5):\n",
    "    raise NotImplementedError()\n",
    "\n",
    "Model.dropout = dropout"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Verify your implementation.\n",
    "\n",
    "The first dropout layer has a `keep_prob` of 1.0, so all activations should survive.\n",
    "\n",
    "The second dropout layer has a `keep_prob` of 0.5, so approximately half of them should be dead."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data = Node(np.random.randint(1,10,size=(10,10)))\n",
    "out0 = dropout(None,keep_prob=1.0)(data)\n",
    "out1 = dropout(None,keep_prob=0.8)(data)\n",
    "\n",
    "# all units should survive\n",
    "assert np.all(out0.value == data.value) \n",
    "\n",
    "# roughly 80% of units should survive\n",
    "np.testing.assert_almost_equal(np.count_nonzero(out1.value)/out1.value.size, 0.8, decimal=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Expected value\n",
    "\n",
    "Say we have a dropout layer with a `keep_prob` of 0.8, so only about 80% of the inputs survive. The expected value of the output is about 80% of that of the input.\n",
    "\n",
    "At test time however, we don't apply dropout - So there's a scaling problem. The units receive test data which have a greater expected value than the train data they learned on.\n",
    "\n",
    "To remedy this, the dropout layer applies the dropout mask, then multiplies the values by $\\frac{1}{keep\\_prob}$ to correct the expected value. Or equivalently, multiply the mask itself by $\\frac{1}{keep\\_prob}$.\n",
    "\n",
    "**Task:**\n",
    "\n",
    "Update your implementation to fix the expected value. Verify your implementation below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "net = Model()\n",
    "data = Node(np.random.randint(1,100,size=(100,100)))\n",
    "out = net.dropout(keep_prob=0.8)(data)\n",
    "\n",
    "# mean of input and output should be similar\n",
    "np.testing.assert_almost_equal(data.value.mean(), out.value.mean(), decimal=0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The cell below executes a training loop which you can use to verify if your model learns appropriately."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# training\n",
    "net = Model()\n",
    "lrate = 0.002\n",
    "batch_size = 75\n",
    "test_losses = []\n",
    "steps=100\n",
    "    \n",
    "for i in range(steps):\n",
    "    minis = np.random.choice(np.arange(len(x_train)),size=batch_size, replace=False)\n",
    "    x_mini = x_train[minis,:]\n",
    "    y_mini = y_train[minis]\n",
    "    loss = net.loss(x_mini,y_mini,train=True)\n",
    "    grads = loss.grad(1)\n",
    "    new_params = { k : net.params[k] - lrate * grads[k]\n",
    "                 for k in grads.keys() }\n",
    "    net.params.update(new_params)               \n",
    "    test_losses.append(net.loss(x_test,y_test,train=False).value.item())\n",
    "\n",
    "# testing\n",
    "pred = np.round(net.forward(x_test,train=False).value.squeeze())\n",
    "np.mean(pred == y_test)\n",
    "plt.plot(test_losses)\n",
    "plt.ylabel('loss on test set')\n",
    "plt.xlabel('iterations');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Literature\n",
    "\n",
    "<table>\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"SRI14\"></a>[SRI14]\n",
    "        </td>\n",
    "        <td>\n",
    "            N. Srivastava, G. Hinton, A. Krizhevsky, I. Sutskever, and R. Salakhutdinov, “Dropout: A Simple Way to Prevent Neural Networks from Overfitting,” Journal of Machine Learning Research, vol. 15, Jun. 2014 pp. 1929-1958, Department of Computer Science, University of Toronto. [Online]. Available: <a href='http://jmlr.org/papers/volume15/srivastava14a/srivastava14a.pdf'>http://jmlr.org/papers/volume15/srivastava14a/srivastava14a.pdf</a> . [Accessed: 20-Jun-2019].\n",
    "        </td>\n",
    "    </tr>\n",
    "        <tr>\n",
    "        <td>\n",
    "            <a name=\"KRI16\"></a>[KRI16]\n",
    "        </td>\n",
    "        <td>\n",
    "            A. Kristiadi, “Implementing Dropout in Neural Net,” Implementing Dropout in Neural Net - Agustinus Kristiadi's Blog, 25-Jun-2016. [Online]. Available: <a href='https://wiseodd.github.io/techblog/2016/06/25/dropout/'>https://wiseodd.github.io/techblog/2016/06/25/dropout/</a>. [Accessed: 20-Jun-2019].\n",
    "        </td>\n",
    "    </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Licenses\n",
    "\n",
    "### Notebook License (CC-BY-SA 4.0)\n",
    "\n",
    "*The following license applies to the complete notebook, including code cells. It does however not apply to any referenced external media (e.g., images).*\n",
    "\n",
    "_Dropout_ <br/>\n",
    "by _Diyar Oktay_ <br/>\n",
    "is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).<br/>\n",
    "Based on a work at https://gitlab.com/deep.TEACHING.\n",
    "\n",
    "\n",
    "### Code License (MIT)\n",
    "\n",
    "*The following license only applies to code cells of the notebook.*\n",
    "\n",
    "Copyright 2018 _Diyar Oktay_\n",
    "\n",
    "Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:\n",
    "\n",
    "The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\n",
    "\n",
    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "deep_teaching_kernel",
   "language": "python",
   "name": "deep_teaching_kernel"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
