var searchData=
[
  ['adam',['Adam',['../classdp_1_1_adam.html',1,'dp']]],
  ['alpha',['alpha',['../classdp_1_1_s_g_d.html#aefb45e9c1ca3aaa11b8e17dc7cacdd9f',1,'dp.SGD.alpha()'],['../classdp_1_1_s_g_d___momentum.html#ab06b99ead1a603967af1e1933c069a38',1,'dp.SGD_Momentum.alpha()'],['../classdp_1_1_r_m_s___prop.html#a83c1d42e3f02142c8cf2c392f4773cfa',1,'dp.RMS_Prop.alpha()'],['../classdp_1_1_adam.html#aea32c42c44685a244378358f13221b8f',1,'dp.Adam.alpha()']]],
  ['animation_5ferror_5fsurface',['Animation_Error_Surface',['../namespace_animation___error___surface.html',1,'']]],
  ['animation_5ferror_5fsurface_2epy',['Animation_Error_Surface.py',['../_animation___error___surface_8py.html',1,'']]],
  ['autodifftest',['AutodiffTest',['../classtest__dp_1_1_autodiff_test.html',1,'test_dp']]]
];
